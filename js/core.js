$(function() {

			var lastName;
			var firstName;
			var updatePostsUrl;

			var hideModal = function(modal){
				modal.fadeOut(700);
				$('.overlay').fadeOut(700);
				modal.find('.field').val('');
			};
			
			var makeProfile = function() {
				$.ajax({
					type: 'GET',
					url: 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/profile'
				}).then(function(response){
					lastName = response.firstName;
					firstName = response.lastName;
					$('.userInfo').text(lastName + ' ' + firstName);
				})
			};

			var changeProfile = function(data) {
				$.ajax({
					type: 'POST',
					url: 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/profile',
					data: data
				}).then(function(){
					makeProfile();
					hideModal($('#editProfileModal'));
				})
			};

			var showPosts = function(response){
				response.forEach(function(item){
					var post = $('<div class="post">');
					var postAuthor = $('<h4>').html(item.user.firstName + ' ' + item.user.lastName + ' <span>said:</span>');
					var postText;				
					if (item.message.length >= 50) {
						var shortPost = item.message.substring(0, 400);
						postText = $('<p>').text(shortPost + '...  ');
					} else {
						postText = $('<p>').text(item.message);
					}
					post.append(postAuthor, postText);
					$('#posts').append(post);
				})
			};

			var getPosts = function(){
				$.ajax({
					type: 'GET',
					url: 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/posts'
				}).then(function(response){
					showPosts(response);
					var lastPostIndex = response.length - 1;
					var lastDatetime = response[lastPostIndex].datetime;
					updatePostsUrl = 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/posts?datetime=' + lastDatetime;
				})
			};

			var updatePosts = function(){
			if(updatePostsUrl){
				$.getJSON(updatePostsUrl).then(function(response){
					if(response.length !== 0){
						showPosts(response);
						var lastPostIndex = response.length - 1;
						var lastDatetime = response[lastPostIndex].datetime;
						updatePostsUrl = 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/posts?datetime=' + lastDatetime;
					}
				});
			}
		};

		var followUser = function() {
			var followedUser = {};
			followedUser.email = $('#emailUser').val();
			$.ajax({
				type: 'POST',
				url: 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/subscribe',
				data: followedUser
			}).then( function() {
				getPosts();
				hideModal($('#followUserModal'));
			})
		};

		var createNewPost = function(){
			var newPost = {};
			newPost.message = $('#newPost').val();
			if (newPost !== '') {
				$.ajax({
					type: 'POST',
					url: 'http://146.185.154.90:8000/blog/gudovaolya@gmail.com/posts',
					data: newPost
				}).then(function(){
					$('#newPost').val('');
				})
			}
		};
		
		makeProfile();
		getPosts();
		setInterval(updatePosts, 5000);

		$('#changeProfile').on('click', function(e) {
			e.preventDefault();
			var userInfo = {};
			userInfo.firstName = $("#firstName").val();
			userInfo.lastName = $("#lastName").val();
			changeProfile(userInfo);
		});

		$('#sendNewPost').on('click', function(e) {
			e.preventDefault();
			createNewPost();
		});

		$('#followUser').on('click', function(e) {
			e.preventDefault();
			followUser();
		});

		$('[data-modal="modal"]').on('click', function(){
			var modalId = $(this).attr('data-target');
			$('.overlay').show();
			$(modalId).show();
		});

		$('.close').on('click', function(){
			hideModal($(this).parent('.modal'));
		});

		
	});